from .event import Event2

class SpeakEvent(Event2):
    NAME = "parler"
    def perform(self):
        if not self.object.has_prop("speakable"):
            self.fail()
            return self.inform("look.failed")
        self.buffer_clear()
        self.buffer_inform("look.actor", object=self.actor.container())
        self.actor.send_result(self.buffer_get())
